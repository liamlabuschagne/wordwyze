Setup
-----
1) I would suggest making a folder called 'store' or something like this and putting my project (everything inside public_html) in there.
2) Install the database tables: go into your phpmyadmin (or mysql terminal instance) and create a database called ww (short for word wyze) and import the ww.sql file to import the tables
3) In the connect.php file, add your user and database password (I use root user but you may be using something else)
4) In the cart.php file on line 18, delete/comment out that line as suggested
5) Uncomment line 20 in cart.php, this is what actually sends the email.

Adding Books/Authors
--------------------
Like shown in the videos (https://thekiwiclick.tk/data/word-wyze.mp4 and https://thekiwiclick.tk/data/wordwyze-v2.mp4), when you add a book or author,
you must upload a file to the /img/[file_name].ext BEFORE adding the book/author.

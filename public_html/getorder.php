<?php
session_start();
if (!isset($_SESSION['cart'])) {
    echo "No items in cart.";
    die();
}
// List books in cart
$sql = "SELECT books.id,title,price,authors.name,authors.id,shipping_base,shipping_inc,authors.email FROM books JOIN authors ON authors.id = author_id WHERE";

$bind_values = [];
$bind_types = "";
$first = true;
foreach ($_SESSION['cart'] as $key => $item) {
    if ($first) {
        $sql .= " books.id = ?";
    } else {
        $sql .= " OR books.id = ?";
    }
    $bind_values[] = $key;
    $bind_types .= "i";
    $first = false;
}
include 'connect.php';
$stmt = $conn->prepare($sql);
$stmt->bind_param($bind_types, ...$bind_values);
$stmt->execute();
$stmt->bind_result($book_id, $title, $price, $author_name, $author_id, $shipping_base, $shipping_inc, $author_email);

$order = ["total_price" => 0.00];
$authors = [];

while ($stmt->fetch()) {
    $quantity = $_SESSION['cart'][$book_id];
    $subtotal = $price * $quantity;
    $book = ["title" => $title, "price" => $price, "quantity" => $quantity, "subtotal" => $subtotal];
    if (!isset($authors[$author_name])) {
        $authors[$author_name] = ["shipping_base" => $shipping_base, "shipping_inc" => $shipping_inc, "email" => $author_email, "books" => [$book], "subtotal" => $price * $quantity + $shipping_inc * $quantity];
    } else {
        $authors[$author_name]["books"][] = $book;
        $authors[$author_name]["subtotal"] += $price * $quantity + $shipping_inc * $quantity;
    }
}

foreach ($authors as $author_name => $value) {
    $authors[$author_name]["subtotal"] += $authors[$author_name]["shipping_base"] - $authors[$author_name]["shipping_inc"];
    $order["total_price"] += $authors[$author_name]["subtotal"];
}
$order["authors"] = $authors;

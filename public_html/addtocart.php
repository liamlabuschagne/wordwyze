<?php
session_start();
// Add cart item
if (isset($_POST['book_id'], $_POST['quantity'])) {
    if (!isset($_SESSION['cart']) || $_SESSION['cart'] == "") {
        $_SESSION['cart'] = array();
    }
    $_SESSION['cart'][$_POST['book_id']] = $_POST['quantity'];
    header("Location: /cart.php");
}

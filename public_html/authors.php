<?php

include 'connect.php';
if (isset($_GET['aid'])) {

    $stmt = $conn->prepare("SELECT name,bio,image,email FROM authors WHERE id = ?");
    $stmt->bind_param('i', $_GET['aid']);
    $stmt->execute();
    $stmt->bind_result($name, $bio, $image, $email);
    $stmt->fetch();

    echo "<h1>" . $name . "</h1>";
    echo "<img src='" . $image . "'>";
    echo "<h2>Biography</h2>";
    echo $bio;
    echo "<h2>Contact</h2>Email: " . $email;

} else {
    header("Location: /");
}

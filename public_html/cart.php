<?php
include 'getorder.php';

if (isset($_POST['name'], $_POST['email'], $_POST['address'], $_POST['city'], $_POST['zip'], $_POST['submit'])) {
    // This is creating the individual email bodies for each author.
    foreach ($order["authors"] as $name => $author) {
        $body = "<p>Dear " . $name . ",</p>";
        $body .= "<p>Shipping base price: $" . number_format($author["shipping_base"], 2) . "</p>";
        $body .= "<p>Shipping price for each extra book: $" . number_format($author["shipping_inc"], 2) . "</p>";
        $body .= "<p>Total: $" . number_format($author["subtotal"], 2) . "</p>";
        $body .= "<table><tr><th>Title</th><th>Price Each</th><th>Quantity</th><th>Subtotal</th></tr>";
        foreach ($author["books"] as $book) {
            $body .= "<tr><td>" . $book["title"] . "</td><td>$" . number_format($book["price"], 2) . "</td><td>" . $book["quantity"] . "</td><td>$" . number_format($book["subtotal"], 2) . "</td></tr>";
        }
        $body .= "</table>";
        $body .= "<p>Customer Name: " . $_POST['name'] . "<br>Customer email: " . $_POST['email'] . "<br> Customer address:<br>" . $_POST['address'] . "<br>" . $_POST['city'] . "<br>" . $_POST['zip'];
        // testing purposes, comment this out
        echo $body;

        // mail($author['email'],"New Order", $body);
    }
} else {
    ?>
    <h1>Cart</h1>
    <a href="/clearcart.php">Clear cart</a>
    <?php
foreach ($order["authors"] as $name => $author) {
        ?>
        <h2><?php echo $name ?></h2>
        <p>Shipping base price: $<?php echo number_format($author["shipping_base"], 2); ?></p>
        <p>Shipping price for each extra book: $<?php echo number_format($author["shipping_inc"], 2); ?></p>
        <p>Subtotal: $<?php echo number_format($author["subtotal"], 2); ?></p>
        <table>
            <tr><th>Title</th><th>Price Each</th><th>Quantity</th><th>Subtotal</th></tr>
        <?php
foreach ($author["books"] as $book) {
            echo "<tr><td>" . $book["title"] . "</td><td>$" . number_format($book["price"], 2) . "</td><td>" . $book["quantity"] . "</td><td>$" . number_format($book["subtotal"], 2) . "</td></tr>";
        }
        ?>
        </table>
                <?php
}
    ?>
<h2>Total: $<?php echo number_format($order["total_price"], 2); ?></h2>
<h2>Place Order</h2>
<p>
    This form will send an email to each author with your order for each one, and your contact information so that you can arrange payment and shipping from there.
</p>
<form method="POST">
    <label for="name">Full Name:</label>
    <input type="text" name="name"><br>
    <label for="email">Email:</label>
    <input type="email" name="email"><br>
    <label for="address">Address Street:</label>
    <input type="text" name="address"><br>
    <label for="city">City/Town:</label>
    <input type="text" name="city"><br>
    <label for="zip">Zip Code:</label>
    <input type="text" name="zip"><br>
    <input type="submit" name="submit" value="Place Order">
</form>
<?php
}
?>
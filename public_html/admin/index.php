<?php
session_start();
if (isset($_POST['submit'], $_POST['password'])) {
    if ($_POST['password'] == "secretpassword") {
        $_SESSION['user_id'] = 1;
    }
}
if (isset($_SESSION['user_id'])) {
    ?>
        <ul>
            <li><a href='./logout.php'>Logout</a></li>
            <li><a href='./addauthor.php'>Add Author</a></li>
            <li><a href='./addbook.php'>Add Book</a></li>
        </ul>
    <?php
} else {
    ?>
        <form method="POST">
            Admin Password:
            <input type="password" name="password">
            <input type="submit" name="submit" value="Log In">
        </form>
    <?php
}

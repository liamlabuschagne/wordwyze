<?php
session_start();
if ($_SESSION['user_id'] != 1) {
    echo "Must be admin.";
    exit;
}
if (isset($_POST['submit'])) {
    $filter = [
        "author_id" => FILTER_VALIDATE_INT,
        "title" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        "description" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        "image" => FILTER_VALIDATE_URL,
        "price" => FILTER_SANITIZE_NUMBER_FLOAT,
        "amazon_link" => FILTER_VALIDATE_URL,
    ];

    $filtered_data = filter_var_array($_POST, $filter);
    foreach ($filtered_data as $key => $value) {
        if ($value == null) {
            echo "Missing information: " . $key;
            exit;
        }
    }

    include '../connect.php';

    $stmt = $conn->prepare("INSERT INTO books (author_id,title,description,image,price,amazon_link) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param('isssds', $filtered_data['author_id'], $filtered_data['title'], $filtered_data['description'], $filtered_data['image'], $filtered_data['price'], $filtered_data['amazon_link']);
    if ($stmt->execute()) {
        echo "Added.";
    } else {
        echo "An error occured.";
    }
} else {
    include '../connect.php';
    // Get authors
    $stmt = $conn->prepare("SELECT id,name FROM authors");
    $stmt->bind_result($id, $name);
    $stmt->execute();
    while ($stmt->fetch()) {
        $authors[] = [$id, $name];
    }
    $stmt->close();
}
?>
<h1>Add Book</h1>
<form method="POST">
    <label for="author">Author</label>
    <select name="author_id">
        <?php
foreach ($authors as $value) {
    echo "<option value='" . $value[0] . "'>" . $value[1] . "</option>";
}
?>
    </select><br>
    <label for="title">Title</label>
    <input type="text" name="title"><br>
    <label for="description">Description</label><br>
    <textarea type="text" name="description"></textarea><br>
    <label for="image">Image URL</label>
    <input type="text" name="image" value="/img/"><br>
    <label for="price">Price (NZD)</label>
    <input type="number" name="price" step="0.01" min="0.01"><br>
    <label for="amazon_link">Amazon Link</label>
    <input type="text" name="amazon_link"><br>
    <input type="submit" name="submit">
</form>
<?php
session_start();
if ($_SESSION['user_id'] != 1) {
    echo "Must be admin.";
    exit;
}

if (isset($_POST['submit'])) {
    $filter = [
        "name" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        "bio" => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        "image" => FILTER_VALIDATE_URL,
        "email" => FILTER_VALIDATE_EMAIL,
        "shipping_base" => FILTER_SANITIZE_NUMBER_FLOAT,
        "shipping_inc" => FILTER_SANITIZE_NUMBER_FLOAT,
    ];

    $filtered_data = filter_var_array($_POST, $filter);
    foreach ($filtered_data as $key => $value) {
        if ($value == null) {
            echo "Missing information: " . $key;
            exit;
        }
    }

    include '../connect.php';

    $stmt = $conn->prepare("INSERT INTO authors (name,bio,image,email,shipping_base,shipping_inc) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param('ssssdd', $filtered_data['name'], $filtered_data['bio'], $filtered_data['image'], $filtered_data['email'], $filtered_data['shipping_base'], $filtered_data['shipping_inc']);
    if ($stmt->execute()) {
        echo "Added.";
    } else {
        echo "An error occured.";
    }
}

?>
<h1>Add Author</h1>
<form method="POST">
    <label for="name">Name</label>
    <input type="text" name="name"><br>
    <label for="bio">Bio</label><br>
    <textarea name="bio"></textarea><br>
    <label for="image">Image URL</label>
    <input type="text" name="image" value="/img/"><br>
    <label for="email">Email</label>
    <input type="email" name="email"><br>
    <label for="shipping_base">Shipping Base Price</label>
    <input type="number" name="shipping_base" step="0.01" min="0.01"><br>
    <label for="shipping_inc">Shipping Increment Price</label>
    <input type="number" name="shipping_inc" step="0.01" min="0.01"><br>
    <input type="submit" name="submit">
</form>

<h1>Books</h1>
<?php
include 'connect.php';
$stmt = $conn->prepare("SELECT books.id,title,description,books.image,price,amazon_link,authors.id,authors.name FROM books JOIN authors ON authors.id = author_id ORDER BY books.id DESC");
$stmt->execute();
$stmt->bind_result($id, $title, $description, $image, $price, $amazon_link, $author_id, $name);
while ($stmt->fetch()) {
    ?>
        <form action="addtocart.php" method="POST">
            <h2><?php echo $title ?></h2>
            <h3>By <a href="/authors.php?aid=<?php echo $author_id; ?>"><?php echo $name; ?></a></h3>
            <img src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
            <p><?php echo $description ?></p>
            <h3>Buy by Email:</h3>
            <input type="hidden" name="book_id" value="<?php echo $id; ?>">
            <label for="quantity">Quantity:</label>
            <input type="number" min="1" name="quantity">
            <input type="submit" name="submit" value="Add to cart">
            <h3>Buy on Amazon</h3>
            <a href="<?php echo $amazon_link; ?>">Buy <?php echo $title; ?> on Amazon</a>
        </form>
<?php
}
?>